package com.hajnasir.plamoon.tab;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.hajnasir.plamoon.R;
import com.hajnasir.plamoon.game_mode;
import com.hajnasir.plamoon.waiting_for_guess_game;
import com.hajnasir.plamoon.waiting_for_xo_activity;


public class RoomFrag extends Fragment {
    Intent intent;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public RoomFrag() {
        // Required empty public constructor
    }


    public static RoomFrag newInstance(String param1, String param2) {
        RoomFrag fragment = new RoomFrag();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public String getData(){
        intent = getActivity().getIntent();
        return intent.getStringExtra("game");

//        game_mode.intent= getActivity().getIntent();
//        return game_mode.intent.getStringExtra("game");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final Intent intent = new Intent(getActivity(), waiting_for_xo_activity.class);
        final Intent intent1 = new Intent(getActivity(), waiting_for_guess_game.class);
        View root = inflater.inflate(R.layout.fragment_room, container, false);
        Button make_button = root.findViewById(R.id.make_button_room);
        final EditText name = root.findViewById(R.id.room_edittext);
        final TextView text = root.findViewById(R.id.room_textview);
        final Button done = root.findViewById(R.id.room_done);
        name.setVisibility(View.INVISIBLE);
        text.setVisibility(View.INVISIBLE);
        done.setVisibility(View.INVISIBLE);
        make_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name.setVisibility(View.VISIBLE);
                text.setVisibility(View.VISIBLE);
                done.setVisibility(View.VISIBLE);
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String room_name = name.getText().toString();
                String game = getData();
                if (game.equals("xo"))
                    startActivity(intent);
                else startActivity(intent1);
            }
        });


        return root;
    }
}