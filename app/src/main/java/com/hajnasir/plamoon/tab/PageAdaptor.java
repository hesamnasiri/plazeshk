package com.hajnasir.plamoon.tab;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class PageAdaptor extends FragmentPagerAdapter {
    public int numOfTabs;

    public PageAdaptor(FragmentManager fm , int numOfTabs){
        super(fm);
        this.numOfTabs = numOfTabs;
    }
    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                return new RankedFrag();
            case 1:
                return new casualFrag();
            case 2:
                return new RoomFrag();
            default:return null;





        }



    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
