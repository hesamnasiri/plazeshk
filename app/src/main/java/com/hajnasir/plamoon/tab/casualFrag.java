package com.hajnasir.plamoon.tab;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.provider.DocumentsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.hajnasir.plamoon.R;
import com.hajnasir.plamoon.game_mode;
import com.hajnasir.plamoon.waiting_for_guess_game;
import com.hajnasir.plamoon.waiting_for_xo_activity;


public class casualFrag extends Fragment {
    Intent intent;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    public casualFrag() {
        // Required empty public constructor
    }

    public String getData(){
        intent = getActivity().getIntent();
        return intent.getStringExtra("game");

//        game_mode.intent= getActivity().getIntent();
//        return game_mode.intent.getStringExtra("game");
    }

    public static casualFrag newInstance(String param1, String param2) {
        casualFrag fragment = new casualFrag();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final Intent intent = new Intent(getActivity(), waiting_for_xo_activity.class);
        intent.putExtra("mode","casual");
        final Intent intent1 = new Intent(getActivity(), waiting_for_guess_game.class);
        intent1.putExtra("mode","casual");
        View root = inflater.inflate(R.layout.fragment_casual, container, false);
        Button start = root.findViewById(R.id.start_button_casual);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String game = getData();
                if (game.equals("xo"))
                    startActivity(intent);
                else startActivity(intent1);
            }
        });


        return root;
    }
}