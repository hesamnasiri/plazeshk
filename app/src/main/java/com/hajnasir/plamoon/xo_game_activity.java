package com.hajnasir.plamoon;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

import static com.hajnasir.plamoon.StartActivity.dis;
import static com.hajnasir.plamoon.StartActivity.dos;

public class xo_game_activity extends AppCompatActivity implements View.OnClickListener {

   private Button[][] buttons = new Button[3][3];
    private boolean player1turn =false;
    boolean player2turn = false;
    private int roundCount;
    private int player1point;
    private int player2point;
   // private TextView textViewplayer1;
    private TextView textViewplayer2;
    Intent intent;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.xo_game);
        //textViewplayer1 = findViewById(R.id.text_view_p1);
        textViewplayer2 = findViewById(R.id.text_view_p2);

        for (int i = 0 ; i < 3 ; i++){
            for (int j = 0 ; j < 3; j++){
                String buttonID = "button_"+ i + j;
                int resID=getResources().getIdentifier(buttonID,"id",getPackageName());
                buttons[i][j]=findViewById(resID);
                buttons[i][j].setOnClickListener(this);

            }
        }


            //Button buttonReset = findViewById(R.id.button_reset);
//            buttonReset.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    resetGame();
//
//                }
//            });

    }



    public String getData(){
        intent = this.getIntent();
        return intent.getStringExtra("mode");

//        game_mode.intent= getActivity().getIntent();
//        return game_mode.intent.getStringExtra("game");
    }



    @Override
    public void onClick(final View v) {
        if (!((Button) v).getText().toString().equals("")){
            return;
        }

        final String mode = getData();
        int i = 0;
        int j = 0;
        switch(v.getId()){
            case R.id.button_00 : i = 0; j = 0;
                break;
            case R.id.button_01 : i = 0; j=1;
                break;
            case R.id.button_02 :i=0; j=2;
                break;
            case R.id.button_10 :i=1; j = 0;
                break;
            case R.id.button_11 :i=1;j=1;
                break;
            case R.id.button_12 :i=1;j=2;
                break;
            case R.id.button_20 :i=2; j = 0;
                break;
            case R.id.button_21 :i=2;j=1;
                break;
            case R.id.button_22 :i=2;j=2;
                break;

        }
            final int i_khodam = i;
            final int j_khodam = j;
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    String javab = dis.readUTF();
                    if(javab.equals("start")){
//                        if (mode.equals("ranked"))
//                            dos.writeUTF("ranked");
//                        else dos.writeUTF("casual");

                        for (int i =0 ; i<9 ; i++){
                        dos.writeInt(i_khodam);
                        dos.writeInt(j_khodam);

//                        String turn = dis.readUTF();
//                        if (turn.equals("x")) {
//                            player1turn = true;
//                        } else {
//                            player2turn = true;
//                        }

//                        String ii = dis.readUTF();
//                        String jj =dis.readUTF();
                        final int i_harif = dis.readInt();
                        final int j_harif = dis.readInt();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                    buttons[i_harif][j_harif].setText("o");
                                    buttons[i_khodam][j_khodam].setText("x");


                            }
                        });}
                    }else {

//                        if (mode.equals("ranked"))
//                            dos.writeUTF("ranked");
//                        else dos.writeUTF("casual");


                        for (int i = 0; i < 9; i++) {
//                        String ii = dis.readUTF();
//                        String jj =dis.readUTF();
                            final int i_harif = dis.readInt();
                            final int j_harif = dis.readInt();


                            dos.writeInt(i_khodam);
                            dos.writeInt(j_khodam);


                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    buttons[i_harif][j_harif].setText("x");
                                    buttons[i_khodam][j_khodam].setText("o");


                                }
                            });


                        }


                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
            thread.start();



//        if (player1turn){
//            ((Button) v).setText("x");
//        }if (player2turn){
//            ((Button) v).setText("o");
//        }

//        Thread thread1 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//
//                    if (player1turn){
//                    buttons[i][j].setText("o");
//                    }else buttons[i][j].setText("x");
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//
//            }
//        });thread1.start();



        roundCount+=2;

        if (checkForWin()){
            if (player1turn){
                player1wins();
            }else {
                player2wins();
            }

        }else if(roundCount==9){
            draw();
        }//else
           // player1turn = !player1turn;


    }

    private boolean checkForWin() {
        String[][] field = new String[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                field[i][j] = buttons[i][j].getText().toString();
            }
        }
        for (int i = 0; i < 3; i++) {
            if (field[i][0].equals(field[i][1])
                    && field[i][0].equals(field[i][2])
                    && !field[i][0].equals("")) {
                return true;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (field[0][i].equals(field[1][i])
                    && field[0][i].equals(field[2][i])
                    && !field[0][i].equals("")) {
                return true;
            }
        }
        if (field[0][0].equals(field[1][1])
                && field[0][0].equals(field[2][2])
                && !field[0][0].equals("")) {
            return true;
        }
        if (field[0][2].equals(field[1][1])
                && field[0][2].equals(field[2][0])
                && !field[0][2].equals("")) {
            return true;
        }
        return false;
    }



    private void player1wins(){
        player1point++;
        Toast.makeText(this,"player 1 wins!",Toast.LENGTH_SHORT).show();
       // updatePointText();
        resetBoard();
    }

    private void player2wins(){
        player2point++;
        Toast.makeText(this,"player 2 wins!",Toast.LENGTH_SHORT).show();
        //updatePointText();
        resetBoard();
    }
    private void draw(){
        Toast.makeText(this,"Draw!",Toast.LENGTH_SHORT).show();
        resetBoard();

    }

//    private void updatePointText(){
//       // textViewplayer1.setText("Player 1:"+player1point);
//        //textViewplayer2.setText("Player 2:"+player2point);
//
//    }


    private void resetBoard(){

        for (int i =0 ; i<3 ; i++){
            for (int j=0 ; j<3 ; j++){
                buttons[i][j].setText("");
            }
        }
        roundCount=0;
        player1turn = true;
    }

    private void resetGame(){
        player1point=0;
        player2point=0;
       // updatePointText();
        resetBoard();

    }


}
