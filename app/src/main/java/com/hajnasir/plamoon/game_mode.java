package com.hajnasir.plamoon;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.hajnasir.plamoon.tab.PageAdaptor;

public class game_mode extends AppCompatActivity {
    public static Intent intent=null;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_mode);
        TabLayout tabLayout = findViewById(R.id.tabBar);
        TabItem ranked = findViewById(R.id.tabChats);
        TabItem casual = findViewById(R.id.tabStatus);
        TabItem room = findViewById(R.id.tabCalls);
        final ViewPager viewPager = findViewById(R.id.viewPager);

        PageAdaptor pageAdaptor=new PageAdaptor(getSupportFragmentManager(),tabLayout.getTabCount() );
        viewPager.setAdapter(pageAdaptor);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



    }
}
