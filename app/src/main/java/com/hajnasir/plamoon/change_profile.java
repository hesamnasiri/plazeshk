package com.hajnasir.plamoon;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.IOException;

import static com.hajnasir.plamoon.StartActivity.dis;
import static com.hajnasir.plamoon.StartActivity.dos;

public class change_profile extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_profile);
         final EditText name_edit = findViewById(R.id.name_editText);
         final EditText bio_edit = findViewById(R.id.bio_editText);
         Button ok_button = findViewById(R.id.ok_button);


       // username_edit.setText(Login_Activity.current_user.getUsername());
//        name_edit.setText(Login_Activity.current_user.getName());
//        bio_edit.setText(Login_Activity.current_user.getBio());

        final Intent intent = new Intent(this , MainActivity.class);


        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final String name = dis.readUTF();
                    final String bio = dis.readUTF();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            name_edit.setText(name);
                            bio_edit.setText(bio);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });

        thread.start();

        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String new_name = name_edit.getText().toString();
                final String new_bio = bio_edit.getText().toString();
                Thread thread1=new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            dos.writeUTF(new_name);
                            dos.writeUTF(new_bio);
                            dos.writeUTF("back");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }


                    }
                });thread1.start();

                startActivity(intent);

            }
        });


    }
//    Login_Activity.current_user.setName(name_edit.getText().toString());
//                Login_Activity.current_user.setBio(bio_edit.getText().toString());
}
