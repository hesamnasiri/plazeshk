package com.hajnasir.plamoon;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

import static com.hajnasir.plamoon.StartActivity.dis;
import static com.hajnasir.plamoon.StartActivity.dos;

public class waiting_for_guess_game extends AppCompatActivity {
    TextView message;
    EditText username;
    Button done;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.waiting_guss_game);
        message = findViewById(R.id.guess_message);
        username = findViewById(R.id.partner_username_guess);
        done = findViewById(R.id.guess_username_done);
        final Intent intent=new Intent(this,guess_the_word_activity.class);
        final Intent intent1=new Intent(this,tell_the_word_activity.class);

   final   Thread listener = new Thread(new Runnable() {
         @Override
         public void run() {
             boolean check_vaziat=true;
             while (check_vaziat) {
                 try {
                 String input= dis.readUTF();
                 if(input.equals("tell")){
                     dos.writeUTF("person1");
                 startActivity(intent1);
                 finish();
                 check_vaziat=false;
                 }else if(input.equals("hads")) {
                   String person1=  dis.readUTF();
                   Log.i("side1",person1);
                     dos.writeUTF("person2");
                     dos.writeUTF(person1);

                    startActivity(intent);
                     finish();
                    check_vaziat=false;
                 }
                 else if(input.equals("wrong")) {
                     runOnUiThread(new Runnable() {
                         @Override
                         public void run() {
                             Toast.makeText(getBaseContext(),"wrong username",Toast.LENGTH_SHORT).show();;
                         }
                     });
                 }
                 else if(input.equals("not ok")) {
                     runOnUiThread(new Runnable() {
                         @Override
                         public void run() {
                             Toast.makeText(getBaseContext(),"friend not online",Toast.LENGTH_SHORT).show();;
                         }
                     });

                 }
                 else if(input.equals("noperson")){
                     runOnUiThread(new Runnable() {
                         @Override
                         public void run() {
                             Toast.makeText(getBaseContext(),"enter friend username",Toast.LENGTH_SHORT).show();;
                         }
                     });
                 }
                 } catch (IOException e) {
                     e.printStackTrace();
                 }
             }
         }
     });
     listener.start();
     done.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
        final String friend_username=     username.getText().toString();
             Thread sender=new Thread(new Runnable() {

                 @Override
                 public void run() {
                     try {
                         dos.writeUTF(friend_username);
                     } catch (IOException e) {
                         e.printStackTrace();
                     }
                 }
             });
             sender.start();
         }
     });


            }

}

