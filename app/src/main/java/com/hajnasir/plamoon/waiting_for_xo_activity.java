package com.hajnasir.plamoon;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

import static com.hajnasir.plamoon.StartActivity.dis;
import static com.hajnasir.plamoon.StartActivity.dos;


public class waiting_for_xo_activity extends AppCompatActivity {
    TextView message;
    EditText username;
    Button done;
    Intent intent;


    public String getData(){
        intent = this.getIntent();
        return intent.getStringExtra("mode");

//        game_mode.intent= getActivity().getIntent();
//        return game_mode.intent.getStringExtra("game");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.waiting_for_game);
        message = findViewById(R.id.xo_message);
        username = findViewById(R.id.partner_username);
        done = findViewById(R.id.xo_done);
        String mode = getData();


        final Intent intent = new Intent(this , xo_game_activity.class);
        intent.putExtra("mode",mode);







        Thread thread2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    String is_ok = dis.readUTF();
                    if (is_ok.equals("wait")){
                        startActivity(intent);
                    }else if (is_ok.equals("start")){
                        startActivity(intent);
                    }else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Toast.makeText(getBaseContext(), "Connection failed. your partner is not in xo game page!", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });thread2.start();





        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean failed = false;
                final String partner_username = username.getText().toString();

                Thread thread1=new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            dos.writeUTF(partner_username);
                            //String answer = dis.readUTF();







                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });thread1.start();





            }
        });








    }
}
