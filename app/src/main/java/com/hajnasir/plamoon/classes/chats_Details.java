package com.hajnasir.plamoon.classes;

public class chats_Details {
    private String lastmessage;
    private  String senttime;
    private String contact;
    public chats_Details(String lastmessage, String senttime, String contact){
        this.contact=contact;
        this.senttime=senttime;
        this.lastmessage=lastmessage;
    }

    public String getContact() {
        return contact;
    }

    public String getLastmessage() {
        return lastmessage;
    }

    public String getSenttime() {
        return senttime;
    }
}
