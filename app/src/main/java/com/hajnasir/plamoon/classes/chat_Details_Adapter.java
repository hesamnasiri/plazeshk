package com.hajnasir.plamoon.classes;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hajnasir.plamoon.R;
import com.hajnasir.plamoon.chatPv_activity;

import java.io.IOException;
import java.util.ArrayList;

import static com.hajnasir.plamoon.StartActivity.dos;

public class chat_Details_Adapter extends RecyclerView.Adapter {

    ArrayList<chats_Details> list_preview;
    public chat_Details_Adapter(ArrayList<chats_Details> chats_details){
        this.list_preview=chats_details;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chat_preview,parent,false);
        return new holder_chat_details(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder,final int position) {
holder_chat_details holder_chd= (holder_chat_details) holder;
holder_chd.bind(list_preview.get(position));
holder_chd.itemView.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    dos.writeUTF("pvchat");
                    dos.writeUTF(list_preview.get(position).getContact());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        Intent intent=new Intent(view.getContext(),chatPv_activity.class);

         view.getContext().startActivity(intent);
    }
});
    }

    @Override
    public int getItemCount() {
        return list_preview.size();
    }


    private class holder_chat_details extends RecyclerView.ViewHolder{
        TextView lastmessage,contact,timetext;
        holder_chat_details(View itemView){
            super(itemView);
            lastmessage=itemView.findViewById(R.id.lastmessage);
            contact=itemView.findViewById(R.id.contact);
            timetext=itemView.findViewById(R.id.time);
        }
        void bind(chats_Details chats_details){
            lastmessage.setText(chats_details.getLastmessage());
            contact.setText(chats_details.getContact());
            timetext.setText(chats_details.getSenttime());
}
    }
}
