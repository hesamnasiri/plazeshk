package com.hajnasir.plamoon.classes;

public class Message {
    private String message;
    private String sender;
    private String sentdate;
    public Message(String message, String sender,String sentdate){
        this.message=message;
        this.sender=sender;
        this.sentdate=sentdate;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public void setSentdate(String sentdate) {
        this.sentdate = sentdate;
    }

    public String getMessage() {
        return message;
    }

    public String getSender() {
        return sender;
    }

    public String getSentdate() {
        return sentdate;
    }
}
