package com.hajnasir.plamoon.classes;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hajnasir.plamoon.R;

import java.util.ArrayList;

public class pv_chats_Adapter extends RecyclerView.Adapter {
    ArrayList<Message> messages_list;
    public  pv_chats_Adapter(ArrayList<Message> messages){
        this.messages_list=messages;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_message,parent,false);
return  new item_message_holder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
     item_message_holder item_message_holder=(item_message_holder) holder;
     item_message_holder.bind(messages_list.get(position));
    }

    @Override
    public int getItemCount() {
        return messages_list.size();
    }
    class item_message_holder extends RecyclerView.ViewHolder{
        TextView text;
        TextView sender;
        TextView time;
        item_message_holder(View itemView){
            super(itemView);
            text=itemView.findViewById(R.id.text);
            sender=itemView.findViewById(R.id.sender);
            time=itemView.findViewById(R.id.time);
        }
        void bind(Message message){
            text.setText(message.getMessage());
            sender.setText(message.getSender());
            time.setText(message.getSentdate());
        }
    }
}
