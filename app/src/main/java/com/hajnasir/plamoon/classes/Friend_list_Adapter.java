package com.hajnasir.plamoon.classes;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.hajnasir.plamoon.R;
import com.hajnasir.plamoon.chatPv_activity;

import java.io.IOException;
import java.util.ArrayList;

import static com.hajnasir.plamoon.StartActivity.dis;
import static com.hajnasir.plamoon.StartActivity.dos;

public class Friend_list_Adapter extends RecyclerView.Adapter {
    ArrayList<String> list_friend;
public Friend_list_Adapter(ArrayList<String> name){
    this.list_friend=name;
}
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friend,parent,false);
        return new holder_friends(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        holder_friends holder_friend= (holder_friends) holder;
        holder_friend.bind(list_friend.get(position));
        holder_friend.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(view.getContext(), chatPv_activity.class);
                final Thread threadStart = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            dos.writeUTF("pvchat");
                            dos.writeUTF(list_friend.get(position));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                threadStart.start();
                intent.putExtra("position",list_friend.get(position)) ;
                view.getContext().startActivity(intent);
                Log.i("test","Ok");
            }
        });

    }

    @Override
    public int getItemCount() {
        return list_friend.size();
    }
    class holder_friends extends RecyclerView.ViewHolder {
        TextView name;
        holder_friends(View itemview){
super(itemview);
name=itemview.findViewById(R.id.name_friend);
        }
        void bind(String s){
            name.setText(s);
        }

    }
}
