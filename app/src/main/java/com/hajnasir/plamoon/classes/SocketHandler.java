package com.hajnasir.plamoon.classes;

import java.net.Socket;

public class SocketHandler {
    private static Socket socket;

    public   Socket getSocket(){
        return socket;
    }

    public    void setSocket(Socket socket){
        SocketHandler.socket = socket;
    }
}
