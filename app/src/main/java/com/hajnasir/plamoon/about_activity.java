package com.hajnasir.plamoon;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

import static com.hajnasir.plamoon.StartActivity.dos;

public class about_activity extends AppCompatActivity {
    TextView about ;
    TextView names;
    Button back;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);

        about = findViewById(R.id.about_textview);
        names = findViewById(R.id.names_textView);
        back = findViewById(R.id.back_button);

        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    dos.writeUTF("back");
                    //dos.writeUTF();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        final Intent intent = new Intent(this,MainActivity.class);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                thread.start();
                startActivity(intent);
            }
        });

    }
}
