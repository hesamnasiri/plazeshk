package com.hajnasir.plamoon;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

import static com.hajnasir.plamoon.StartActivity.dis;
import static com.hajnasir.plamoon.StartActivity.dos;

public class guess_the_word_activity extends AppCompatActivity {
    Intent home=new Intent(this,MainActivity.class);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        final Intent intent1=new Intent(this,tell_the_word_activity.class);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.guess_the_word);
        TextView game_name = findViewById(R.id.game_name_textView);
        //TextView word_textview = findViewById(R.id.word_textView);
        TextView letter_textview = findViewById(R.id.letter_textView);
      //  EditText word_edittext = findViewById(R.id.word_editText);
        final Button ok = findViewById(R.id.ok_button_letter);
        final EditText letter_edittext = findViewById(R.id.letter_editText);
        final Button[] buttons = new Button[9];
        for (int i = 0; i < 9; i++) {
            String buttonID = "button_" + i ;
            int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
            buttons[i] = findViewById(resID);
            buttons[i].setVisibility(View.INVISIBLE);

        }
        // harf hads mizaneh

        final Button[] button_roshan_kolli = new Button[9];
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String harf=  letter_edittext.getText().toString();
                //Toast.makeText(getBaseContext(),harf,Toast.LENGTH_SHORT).show();
                Thread sender=new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            dos.writeUTF(harf);
                            Log.i("sent",harf);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                sender.start();

            }
        });

        Thread Listener =new Thread(new Runnable() {
    @Override
    public void run() {
        try {

             final String wordinput=dis.readUTF();
             dos.writeUTF(wordinput);
             Log.i("word",wordinput);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (int i=0 ; i<wordinput.length();i++){
                        button_roshan_kolli[i]=buttons[i];
                        button_roshan_kolli[i].setVisibility(View.VISIBLE);
                    }
                    final char[] javab_array = new char[wordinput.length()];
                    for (int k=0 ; k<javab_array.length;k++){
                        javab_array[k]= wordinput.charAt(k);

                    }
                }
            });


        } catch (IOException e) {
            e.printStackTrace();
        }
        while (true){
            try {
                boolean send=false;

        final String result  =   dis.readUTF();
        if(result.equals("ok")){
             final String harf=   dis.readUTF();
             final int place=dis.readInt();
             runOnUiThread(new Runnable() {
                 @Override
                 public void run() {
                     button_roshan_kolli[place].setText(harf);
                     Toast.makeText(getBaseContext(),"ok",Toast.LENGTH_SHORT).show();
                 }
             });

        }
        else if(result.equals("wrong")) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(),result,Toast.LENGTH_SHORT).show();
                }
            });

        }
        else if(result.equals("start")) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(), "game start ", Toast.LENGTH_SHORT).show();
                }
            });
        }
        else if(result.equals("endround1")){
            startActivity(intent1);
            finish();
            break;
        }
        else if (result.equals("loseround1")){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(),"lose",Toast.LENGTH_SHORT).show();
                }
            });
            startActivity(intent1);
            finish();
            break;
        }
        else if(result.equals("end")){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(),"Draw",Toast.LENGTH_SHORT).show();
                    startActivity(home);
                    finish();
                }
            });
        }
        else if(result.equals("lose")){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getBaseContext(),"YOU LOSE",Toast.LENGTH_SHORT).show();
                    startActivity(home);
                    finish();
                }
            });
        }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
});
Listener.start();

    }
}
