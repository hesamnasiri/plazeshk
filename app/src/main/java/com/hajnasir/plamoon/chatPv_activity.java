package com.hajnasir.plamoon;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hajnasir.plamoon.classes.Message;
import com.hajnasir.plamoon.classes.pv_chats_Adapter;

import java.io.IOException;
import java.util.ArrayList;

import static com.hajnasir.plamoon.StartActivity.dis;
import static com.hajnasir.plamoon.StartActivity.dos;

public class chatPv_activity extends AppCompatActivity {

    EditText chatbox;
    Button sendbutton;
    Intent intent;
    int position;
    String  title;
    ArrayList<Message> data_caht=new ArrayList<>();
    private Thread thread1=new Thread();
Intent intentloop=new Intent(this,chatPv_activity.class);
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatpv);
        chatbox=findViewById(R.id.chatbox);
        sendbutton=findViewById(R.id.sendtext);
        final   RecyclerView  recyclerView=findViewById(R.id.recyclerView2);
        //   final String[] title = new String[1];
        final Thread thread =new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final String   title =dis.readUTF();
                    int num_chat=dis.readInt();
                    for (int i=0; i<num_chat; i++){
                        String text =dis.readUTF();
                        String sender=dis.readUTF();
                        String time=dis.readUTF();
                        Message message=new Message(text,sender,time);
                        data_caht.add(message);
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            getSupportActionBar().setTitle(title);
                            LinearLayoutManager mLayoutManager = new LinearLayoutManager(getBaseContext());
                            //  mLayoutManager.setReverseLayout(true);
                            mLayoutManager.setStackFromEnd(true);
                            RecyclerView.LayoutManager layoutManager;
                            layoutManager = new LinearLayoutManager(getBaseContext());
                            recyclerView.setLayoutManager(mLayoutManager);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        Message testmessage =new Message("send some thing","send","date");

        data_caht.add(testmessage);
        final pv_chats_Adapter Adapter=new pv_chats_Adapter(data_caht);
        recyclerView.setAdapter(Adapter);
        sendbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String text= chatbox.getText().toString();
                final String time="10";
                thread1=new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //  while (!Thread.currentThread().isInterrupted()){

                        try {
                            dos.writeUTF("sendtext");
                            dos.writeUTF(text);
                            dos.writeUTF(time);
                            dis.readUTF();

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    //}
                    }
                });
                thread1.start();
                Adapter.notifyDataSetChanged();
            }
        });




    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            thread1.interrupt();
            Thread thread=new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        dos.writeUTF("backpeople");
                        dos.writeUTF("back");
                        dos.writeUTF("game");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back_button, menu);
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==R.id.back_button){
            thread1.interrupt();
            Intent intent3 = new Intent(this, MainActivity.class);
            Thread thread =new Thread(new Runnable() {
                @Override
                public void run() {
                    try {

                dos.writeUTF("exit");
                dos.writeUTF("back");
                        finish();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
            finish();
            startActivity(intent3);

        }
        return super.onOptionsItemSelected(item);
    }
}
