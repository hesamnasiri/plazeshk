package com.hajnasir.plamoon.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.hajnasir.plamoon.R;
import com.hajnasir.plamoon.game_mode;
import com.hajnasir.plamoon.waiting_for_guess_game;
import com.hajnasir.plamoon.waiting_for_xo_activity;

import java.io.IOException;

import static com.hajnasir.plamoon.StartActivity.dos;

public class game_fragment extends Fragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         View root =inflater.inflate(R.layout.fragment_game,container,false);

        Button xo_button = root.findViewById(R.id.xo_button);
        Button guess_game = root.findViewById(R.id.guess_game);

//        final Intent intent_XO = new Intent(getActivity(), waiting_for_xo_activity.class);
//        final Intent intent_guess= new Intent(getActivity(), waiting_for_guess_game.class);

        final Intent intent_XO = new Intent(getActivity(), game_mode.class);
        intent_XO.putExtra("game","xo");
        final Intent intent_guess= new Intent(getActivity(), game_mode.class);
        intent_guess.putExtra("game","guess");

//=======
//        final Intent intent_XO = new Intent(getActivity(), waiting_for_xo_activity.class);
//        final Intent intent_guess= new Intent(getActivity(), waiting_for_guess_game.class);
////=======
////        final Intent intent = new Intent(getActivity(), game_mode.class);
////        intent.putExtra("game","xo");
////        final Intent intent1= new Intent(getActivity(), game_mode.class);
////        intent1.putExtra("game","guess");

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Game");
        xo_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Thread thread1=new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            dos.writeUTF("xo");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });thread1.start();
                startActivity(intent_XO);
            }


        });

        guess_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread thread2=new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            dos.writeUTF("guessgame");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });thread2.start();
                startActivity(intent_guess);
            }
        });


         return root;
    }
}
