package com.hajnasir.plamoon.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.hajnasir.plamoon.R;
import com.hajnasir.plamoon.StartActivity;
import com.hajnasir.plamoon.classes.Message;
import com.hajnasir.plamoon.classes.chat_Details_Adapter;
import com.hajnasir.plamoon.classes.chats_Details;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import static com.hajnasir.plamoon.StartActivity.dis;
import static com.hajnasir.plamoon.StartActivity.dos;
import static com.hajnasir.plamoon.StartActivity.socket;

public class chatfragment extends Fragment {

    ArrayList<chats_Details> chats_preview_list =new ArrayList<>();
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Chat");

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_chat,container,false);

        final RecyclerView recyclerView=root.findViewById(R.id.chat_preview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        final chat_Details_Adapter  adapter=new chat_Details_Adapter(chats_preview_list);
        //recyclerView.setAdapter(adapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    dos.writeUTF("getList");
                  int size=   dis.readInt();
for (int i=0; i<size; i++){
  String name=  dis.readUTF();
  String last= dis.readUTF();
  String time=  dis.readUTF();
    chats_Details newDetail=new chats_Details(last,time,name);
    chats_preview_list.add(newDetail);
    getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {
            recyclerView.setAdapter(adapter);
        }
    });
}

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
thread.start();

//        chats_Details test=new chats_Details("salam","10","hesam");
//        chats_Details test1=new chats_Details("salam","11","ali");
//        chats_Details test2=new chats_Details("salam","12","reza");
//        chats_Details test3=new chats_Details("salam","13","mohammad");
//        chats_preview.add(test); chats_preview.add(test1);  chats_preview.add(test2);   chats_preview.add(test);chats_preview.add(test3);
//        chats_preview.add(test2);  chats_preview.add(test3);  chats_preview.add(test1);  chats_preview.add(test2);  chats_preview.add(test);


        return root;
    }
}