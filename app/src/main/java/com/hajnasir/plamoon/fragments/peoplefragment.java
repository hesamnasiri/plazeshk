package com.hajnasir.plamoon.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hajnasir.plamoon.MainActivity;
import com.hajnasir.plamoon.R;
import com.hajnasir.plamoon.StartActivity;
import com.hajnasir.plamoon.classes.Friend_list_Adapter;
import com.hajnasir.plamoon.classes.chat_Details_Adapter;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Objects;

import static com.hajnasir.plamoon.StartActivity.dis;
import static com.hajnasir.plamoon.StartActivity.dos;
import static com.hajnasir.plamoon.StartActivity.socket;

public class peoplefragment extends Fragment {
    FloatingActionButton myFab;
    EditText Enterusername;
    Button Add_friend;
    String input_username_friend;
ArrayList<String> friends=new ArrayList<>();
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_people, container, false);
        myFab = root.findViewById(R.id.floating_action_button);
        Enterusername = root.findViewById(R.id.usernamefriend_input);
        Enterusername.setVisibility(View.INVISIBLE);
        Add_friend = root.findViewById(R.id.buttonaddfriend);
        Add_friend.setVisibility(View.INVISIBLE);
     final     RecyclerView recyclerView=root.findViewById(R.id.recycler_friend);
        final Friend_list_Adapter adapter=new Friend_list_Adapter(friends);

        final Thread start=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    dos.writeUTF("getList");
                    int num=dis.readInt() ;
                    for (int i=0; i<num; i++){
                      String temp=  dis.readUTF();
                      friends.add(temp);
                        Log.i("teest",friends.toString()) ;
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                recyclerView.setAdapter(adapter);
                            }
                        });
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        start.start();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        recyclerView.setItemAnimator(new DefaultItemAnimator());
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("People");

        myFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Enterusername.setVisibility(View.VISIBLE);
                Add_friend.setVisibility(View.VISIBLE);
            }
        });
        Add_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            input_username_friend = Enterusername.getText().toString();
               // new search_friend(getActivity().getApplicationContext()).execute();

                Thread thread=new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            dos.writeUTF("addfriend");
                            dos.writeUTF(input_username_friend);
                            final String result= StartActivity.dis.readUTF();
                            if(getActivity()!=null) {
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (result.equals("done")) {
                                            adapter.notifyDataSetChanged();
                                            Toast.makeText(getContext(), "done", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(getContext(), "username is invalid", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
            }
        });

        return root;
    }


    class search_friend extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                dos.writeUTF("addfriend");
                dos.writeUTF(input_username_friend);
                String result = StartActivity.dis.readUTF();
                if (result.equals("done")) {
                    return true;
                } else {
                    return false;

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean) {
                Toast.makeText(getContext(), "done", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "dont ok", Toast.LENGTH_SHORT).show();
            }
            super.onPostExecute(aBoolean);
        }
    }

}

