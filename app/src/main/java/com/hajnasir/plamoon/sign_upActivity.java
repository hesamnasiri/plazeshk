package com.hajnasir.plamoon;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
//
//import static com.hajnasir.plamoon.classes.Server.datacenter;

public class sign_upActivity extends AppCompatActivity {
    EditText user_editText;
    EditText pass_editText;
    EditText name_editText;
    EditText bio_editText;
    Button submit_button;
    ImageView imageView;
    private int PICK_IMAGE_REQUEST = 1;
DataInputStream dis;
DataOutputStream dos;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        user_editText = findViewById(R.id.editTextTextPersonName);
        user_editText.setBackgroundResource(R.drawable.normal_textview);
        pass_editText = findViewById(R.id.editTextTextPassword);
        pass_editText.setBackgroundResource(R.drawable.normal_textview);
        name_editText = findViewById(R.id.name_editText);
        name_editText.setBackgroundResource(R.drawable.normal_textview);
        bio_editText = findViewById(R.id.bio_editTextText);
        bio_editText.setBackgroundResource(R.drawable.normal_textview);
        submit_button = findViewById(R.id.submitbutton);
        imageView = findViewById(R.id.imageView2);
        imageView.setImageResource(R.drawable.ic_chat_buttom_foreground);

        final Intent intent = new Intent(this , MainActivity.class);

        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Thread thread=new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            StartActivity.dos.writeUTF(user_editText.getText().toString());
                            if(pass_editText.getText().toString().length()<8){
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        pass_editText.setBackgroundResource(R.drawable.error_textview);
                                    }
                                });
                            }else {
                                StartActivity.dos.writeUTF(pass_editText.getText().toString());
                            }
                           StartActivity.dos.writeUTF(name_editText.getText().toString());
                           StartActivity.dos.writeUTF(bio_editText.getText().toString());
                           StartActivity.dos.flush();
                        String result=   StartActivity.dis.readUTF();
                        if (result.equals("signupok")){
                            startActivity(intent);
                        }
                        else {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    user_editText.setBackgroundResource(R.drawable.error_textview);
                                    user_editText.getText().clear();
                                    user_editText.setHint("Username already in use");
                                }
                            });

                        }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                if (user_editText.getText().toString().isEmpty() || pass_editText.getText().toString().isEmpty()) {
                    Toast.makeText(getBaseContext(), "Error many field is empty ", Toast.LENGTH_LONG).show();
                }
                else
                {
                    thread.start();
                    //imageView.getDrawable();
                }
            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseImage();
            }
        });

    }

    public void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));


                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
