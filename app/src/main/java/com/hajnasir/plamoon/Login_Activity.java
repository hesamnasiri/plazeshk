package com.hajnasir.plamoon;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

import static com.hajnasir.plamoon.StartActivity.dos;
//
//import static com.hajnasir.plamoon.MainActivity.current_user;

public class Login_Activity extends AppCompatActivity {
    Button b1;
    EditText username;
    String input_username;
    EditText password;
    String input_password;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        b1=findViewById(R.id.checklogin);
        username=findViewById(R.id.enterusername);
        password=findViewById(R.id.enterpassword);
        final Intent  intentmain = new Intent(this,MainActivity.class);
        b1.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    dos.writeUTF(input_username);
                    dos.writeUTF(input_password);
                    final String result = StartActivity.dis.readUTF();
                    if(result.equals("loginok")){
                         startActivity(intentmain);
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (result.equals("loginok")) {
                                Toast.makeText(getBaseContext(), "good", Toast.LENGTH_LONG).show();

                            } else {
                                username.setBackgroundResource(R.drawable.error_textview);
                                password.setBackgroundResource(R.drawable.error_textview);
                                Toast.makeText(getBaseContext(), "username or password is wrong", Toast.LENGTH_LONG).show();
                            }
                        }
                    });


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        input_username=username.getText().toString();
        input_password=password.getText().toString();
        thread.interrupt();
        if(input_password.isEmpty()||input_username.isEmpty()){
            Toast.makeText(getBaseContext(),"dont empty",Toast.LENGTH_LONG).show();
        }else {
                thread.start();
        }
    }
});


}
    @Override
    public void onBackPressed() {

        finish();
        Intent intent = new Intent(this, MainActivity.class);
        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {

                try {

                    dos.writeUTF("backpeople");
                    dos.writeUTF("back");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        startActivity(intent);
    }
 class login extends  AsyncTask<Void,String,Boolean>{


     @Override
     protected Boolean doInBackground(Void... voids) {

         return null;
     }

     @Override
     protected void onPostExecute(Boolean result) {
         if(result) {

         }
         else
         {

         }

     }
 }
}
