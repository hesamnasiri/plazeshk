package com.hajnasir.plamoon;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.hajnasir.plamoon.classes.SocketHandler;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.Socket;

public class StartActivity extends AppCompatActivity {
    Socket socketfirst;
  public static SocketHandler socket=new SocketHandler();
    public  static   DataOutputStream dos;
    public static   DataInputStream dis;
  public  static   InputStream inputStream;
 public  static    ObjectInputStream obj;
    TextView welcome_text;
    Button login_button ;
    Button signup_button;
     Intent intent_signup ;
     Intent intent_Login;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_start);
        login_button=findViewById(R.id.loginbutton);
        signup_button=findViewById(R.id.signupbutton);
        welcome_text=findViewById(R.id.welcometextview);
        intent_signup = new Intent(this, sign_upActivity.class);
        intent_Login=new Intent(this,Login_Activity.class);
        Thread thread =new Thread(new Runnable() {
            @Override
            public void run() {
                try {


                    socketfirst=new Socket("192.168.1.105",3000);



                    socket.setSocket(socketfirst);
                    dis=new DataInputStream(socket.getSocket().getInputStream());
                    dos=new DataOutputStream(socket.getSocket().getOutputStream());
//                    inputStream=socket.getSocket().getInputStream();
//                    obj=new ObjectInputStream(inputStream);
                    login_button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                                Thread thread1=new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            for (int i =0 ; i<1 ; i++){
                                                dos.writeUTF("login");
                                                dos.flush();
                                            }

                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                thread1.start();

                            startActivity(intent_Login);
                        }
                    });
                    signup_button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                             Thread thread1=new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        for (int i =0 ; i<1 ; i++){
                                        dos.writeUTF("signup");
                                        dos.flush();
                                        }

                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            thread1.start();

                            startActivity(intent_signup);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
        super.onCreate(savedInstanceState);



    }

}
