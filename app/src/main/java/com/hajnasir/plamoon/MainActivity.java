package com.hajnasir.plamoon;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.hajnasir.plamoon.fragments.chatfragment;
import com.hajnasir.plamoon.fragments.game_fragment;
import com.hajnasir.plamoon.fragments.homefragment;
import com.hajnasir.plamoon.fragments.peoplefragment;

import java.io.IOException;
import java.net.Socket;

import static com.hajnasir.plamoon.StartActivity.dos;

public class MainActivity extends AppCompatActivity {
BottomNavigationView bottomNavigationView;

  //  public static Users current_user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigationView = this.findViewById(R.id.navigation_bar_bottom);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_chat:
                        Thread thread1=new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    dos.writeUTF("back");
                                    dos.writeUTF("chat");
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        thread1.start();
                        chatfragment chatfragment = new chatfragment();
                        openfragment(chatfragment);
                        break;
                    case R.id.nav_game:
                        Thread thread2=new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    dos.writeUTF("back");
                                    dos.writeUTF("game");
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        thread2.start();
                        game_fragment gameFragment = new game_fragment();
                        openfragment(gameFragment);
                        break;
                    case R.id.nav_home:
                        homefragment homefragment = new homefragment();
                        openfragment(homefragment);
                        break;
                    case R.id.nav_people:
                        Thread thread3=new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    dos.writeUTF("back");
                                    dos.writeUTF("people");
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        thread3.start();
                        peoplefragment peoplefragment = new peoplefragment();
                        openfragment(peoplefragment);
                        break;
                }
                return true;


            }
        });
        TextView textView=findViewById(R.id.testtext);


    }

       class change_fragment extends AsyncTask<String,Void,Boolean>{
           @Override
           protected Boolean doInBackground(String... strings) {
               String test= String.valueOf(strings);
               return null;
           }
       }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_toolbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_profile) {
            // do something here

            Thread thread2=new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        dos.writeUTF("back");
                        dos.writeUTF("profile");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread2.start();
             Intent intent2 = new Intent(this, profileActivity.class);
            startActivity(intent2);

        }

        if (id == R.id.settings) {
            // do something here
            Intent intent3 = new Intent(this, settings_activity.class);
            Thread thread =new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        dos.writeUTF("back");
                        dos.writeUTF("settings");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
            startActivity(intent3);

        }



        return super.onOptionsItemSelected(item);
    }


    public  void openfragment(Fragment fra){
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame,fra);
        transaction.commit();
    }
}