package com.hajnasir.plamoon;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

import static com.hajnasir.plamoon.StartActivity.dis;
import static com.hajnasir.plamoon.StartActivity.dos;

public class profileActivity extends AppCompatActivity {



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        final TextView username_view = findViewById(R.id.username_textView);
        final TextView name_view = findViewById(R.id.name_textView);
        final TextView bio_view = findViewById(R.id.bio_textView);
        Button change_button = findViewById(R.id.change_button) ;



        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    dos.writeUTF("getdata");
                    final String username = dis.readUTF();
                    final String name = dis.readUTF();
                    final String bio = dis.readUTF();


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            username_view.setText(username);
                            if (name != null){
                                name_view.setText(name);
                            }else name_view.setText("-");

                            if (bio != null){
                                bio_view.setText(bio);
                            }else bio_view.setText("-");
                        }
                    });



                } catch (IOException e) {
                    e.printStackTrace();
                }



            }
        });
        thread.start();








        final Intent intent = new Intent(this,change_profile.class);
        final Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    dos.writeUTF("change");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });







        change_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                thread1.start();
                startActivity(intent);
            }
        });

    }
}
