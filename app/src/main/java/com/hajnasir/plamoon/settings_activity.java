package com.hajnasir.plamoon;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

import static com.hajnasir.plamoon.StartActivity.dos;


public class settings_activity extends AppCompatActivity {
    Button about;
    Button logout;


    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        about = findViewById(R.id.about_us);
        logout=findViewById(R.id.logout);


        final Intent intent = new Intent(this,about_activity.class);
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread thread=new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            dos.writeUTF("aboout");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
                startActivity(intent);
            }
        });
        final Intent intent1= new Intent(this , StartActivity.class);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread thread =new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            dos.writeUTF("logout");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
                startActivity(intent1);
            }
        });



    }
}
